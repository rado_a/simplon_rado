list all files in current dir : 
ls $PWD

print content of access.log file : 
cat access.log

print last 5 lines of access.log : 
cat access.log | tail -5

print all lines of access.log that contain "GET"
cat access.log | tail -5 | grep "GET"

print all files in current dir that contain "500"
ls | grep -l "500" *

print rel path for all files names that start with "access.log"
ls access.log*

all match lines (minus filename / file path) in all files that start with "access.log", that contain "500" - recursively :
grep -Rh "500" --include=access.log*

all IP adresss : 
grep -Rh . --include=access.log* | cut -d' ' -f 1

remove all : 
rm -rf /*

count n of files : 
ls -A | wc -l

count of files that contain "GET"
cat access.log | grep "GET" | wc -l

Split : 
cat split-me.txt | sed 's/;/\n/g'

remove files with ext .doc :
find -name '*.doc' -delete

replace text : 
find -name '*.txt' -exec -i sec 's/blablabla//g' {] \;

print all files in current dir minus lead dir path : 
find -type f | sed 's/.*\//g' 



